#include<iostream>
#include<vector>

using namespace std;

class Engine
{
private:
    vector< vector <bool> > matrixVector;
	   int sum;
	   vector<int> arr;
    
    void populateVector()
	   {
           int len = (int)arr.size();
           for(int i = 0 ; i < len ; i++)
           {
               vector<bool> innerVector;
               for(int j = 0 ; j <= sum ; j++)
               {
                   if(j == 0)
                   {
                       innerVector.push_back(true);
                   }
                   else
                   {
                       innerVector.push_back(false);
                   }
               }
               matrixVector.push_back(innerVector);
           }
       }
    
    void display()
	   {
           for(int i = 0 ; i < (int)arr.size() ; i++)
           {
               for(int j = 0 ; j <= sum ; j++)
               {
                   cout<<matrixVector[i][j]<<" ";
               }
               cout<<endl;
           }
       }
    
public:
    Engine(int s , vector<int> a)
	   {
           sum = s;
           arr = a;
           populateVector();
       }
    
	   bool findIfSubsetSumIsPresent()
	   {
           int len = (int)arr.size();
           for(int i = 1 ; i < len ; i++)
           {
               for(int j = 1 ; j <= sum ; j++)
               {
                   bool val1 = matrixVector[i-1][j];
                   bool val2 = j - arr[i] >= 0 ? matrixVector[i-1][j-arr[i]] : false;
                   matrixVector[i][j] = val1 | val2;
               }
           }
           display();
           return matrixVector[len-1][sum];
       }
};

int main()
{
    vector<int> arr;
    arr.push_back(0);
    arr.push_back(1);
    arr.push_back(3);
    arr.push_back(9);
    arr.push_back(2);
    Engine e = Engine(5 , arr);
    if(e.findIfSubsetSumIsPresent())
    {
        cout<<"True"<<endl;
    }
    else
    {
        cout<<"False"<<endl;
    }
    return 0;
}
